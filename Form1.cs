﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Numerics;

namespace Shamir
{
    public partial class Form1 : Form
    {
        private Dictionary<int, BigInteger[]> parts;

        private string rightText = "";
        private string leftText = "";

        Encoding unicode = Encoding.GetEncoding("windows-1251");

        public Form1()
        {
            InitializeComponent();
        }

        // клик для разделения секрета
        private void button1_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = "";
            // считываем секрет с поля
            String my_secret = textBox1.Text;

            // считываем значение числа P (почему-то для чисел меньше сотни работает неправильно...)
            // А еще оно должно быть простым, это важно
            BigInteger my_square = new BigInteger();
            my_square = BigInteger.Parse(textBox2.Text);

            // n - на сколько человек делим секрет, k - сколько человек может восстановить секрет
            int n = 0, k = 0;

            k = Convert.ToInt32(textBox3.Text);
            n = Convert.ToInt32(textBox4.Text);

            rightText = String.Format(
                "Пусть нужно разделить секрет «{0}» между {1} сторонами. " +
                "При этом любые {2} стороны должны иметь возможность восстановить этот секрет. " +
                "Возьмём простое число p={3}. Построим многочлен степени k-1={4}-1={5}.\n\n", my_secret, n, k,
                my_square, k,
                k - 1);

            // словарь для "теней"
            parts
                = Shamir(my_secret, my_square, n, k);

            int i = 0;
            rightText += "\n\n";
            String output = "";
            foreach (var part in parts)
            {
                output += part.Key + " : ";
                for (int j = 0; j < part.Value.Length; j++)
                {
                    output += String.Format("({0}, {1}),", part.Key, part.Value[j]);
                }
                output += "\n";
                i++;
            }
            rightText += output;
            rightText += String.Format(
                "\n\nПосле этого ключи (вместе с их номером) раздаются сторонам. " +
                "Случайные коэффициенты и сам секрет  «забываются»." +
                "Теперь любые {0} участников смогут восстановить многочлен и все его коэффициенты, включая последний из них — разделённый секрет.",
                k);
            richTextBox1.Text = rightText;
        }

        // обновить текст многочлена вверху
        private void update_f()
        {
            if (textBox3.Text == "") return;

            string line = "( ";
            for (int j = 1; j < Convert.ToInt32(textBox3.Text); j++)
            {
                line = line + "S[" + j + "]*x^" + (Convert.ToInt32(textBox3.Text) - j) + " + ";
            }

            line = line + textBox1.Text + " ) mod P";
            lagrange.Text = line;
        }

        // Восстановление секрета
        private void button2_Click(object sender, EventArgs e)
        {
            BigInteger p
                = BigInteger.Parse(textBox2.Text);

            String result = deShamir(parts, p).ToString();
            richTextBox1.Text = rightText + leftText +
                                String.Format("\n\nВ конечном итоге, переведя из байтов в символы, получим секрет {0}",
                                    result);
        }

        // метод, следящий за обновлением полей 
        private void textBox_TextChanged(object sender, EventArgs e)
        {
            update_f();
        }


        //--------------------------------------------------------------
        // разделение секрета Шамира
        //--------------------------------------------------------------
        private Dictionary<int, BigInteger[]> Shamir(String secret, BigInteger p, int n, int k)
        {
            BigInteger[] Koeff = new BigInteger[k - 1];

            Dictionary<int, BigInteger[]> total_keys = new Dictionary<int, BigInteger[]>();
            rightText += "F(x) = (";

            // --------------------------------------
            // создание коэффициентов многочлена a[i].
            // --------------------------------------
            for (int i = 0; i < k - 1; i++)
            {
                Koeff[i] = MathMod(BigInteger.Abs(getRandom((i + 1) * 2)), p);
                rightText += Koeff[i] + "*x^" + (k - 1 - (i));
                if (i != k - 1 - 1)
                {
                    rightText += "+";
                }
            }
            rightText += "+secret_byte) mod " + p + ".";

            rightText += String.Format(
                "\n\nВ этом многочлене secret_byte - это код одного символа разделяемого секрета, " +
                "а остальные коэффициенты - некоторые случайные числа, которые нужно будет «забыть» " +
                "после того, как процедура разделения секрета будет завершена. Теперь вычисляем координаты {0} различных точек для каждого символа секрета.",
                n);

            // В эту переменную сохраняются значения S[i]*i^t
            // Т.е. в S[i]*x^t подставляем значение i
            BigInteger powered = new BigInteger();

            // Здесь по итогу будет храниться S(x)
            // Изначально эта переменная равна нулю
            BigInteger result = new BigInteger();

            // заполнение массива кодов символов
            byte[] secret_bytes = unicode.GetBytes(secret);

            // итерации по участникам
            for (int i = 0; i < n; i++)
            {
                BigInteger[] secret_values = new BigInteger[secret.Length];
                // итерации по длине секрета
                for (int t = 0; t < secret.Length; t++)
                {
                    // здесь S(x) становится равно m
                    result = result + secret_bytes[t];

                    for (int j = 1; j < k; j++) // вычисление величины многочлена для i, где i = 1 .. n
                    {
                        // на каждой итерации прибавляем к S(x) 
                        // значение S[i]*x^t
                        powered = BigInteger.Pow(i + 1, k - j);
                        result += Koeff[j - 1] * powered;
                    }

                    result = MathMod(result, p);

                    secret_values[t] = result;
                    result = 0;
                }

                if (!total_keys.ContainsKey(i + 1))
                {
                    total_keys.Add(i + 1, secret_values);
                }
            }
            return total_keys;
        }

        //--------------------------------------------------------------
        // восстановление секрета
        //--------------------------------------------------------------
        private String deShamir(Dictionary<int, BigInteger[]> kv_pairs, BigInteger p)
        {
            List<byte> recoveredSecret = new List<byte>();
            byte[] recoveredSecretArray;

            BigInteger high_part = new BigInteger(1);
            BigInteger low_part = new BigInteger(1);

            leftText =
                "\n\nПостроив интерполяционный член Лагранжа, будем поэтапно находить значения байтов искомого ключа:\n";

            // first_kv - тень номер 1
            // second_kv -тень номер 2
            BigInteger secret = new BigInteger(0);
            try
            {
                List<int> members = textBox6.Text.Split(',').Select(h => Int32.Parse(h)).ToList();
                for (int i = 0; i < kv_pairs[1].Length; i++)
                {
                    leftText += String.Format("\nДля {0} символа: ", i + 1);
                    secret = new BigInteger(0);
                    foreach (var first_kv in members)
                    {
                        foreach (var second_kv in members)
                        {
                            if (first_kv != second_kv)
                            {
                                high_part = high_part * ((-1) * second_kv // числитель
                                            ); // -1 потому, что x - xj, но т.к. ищем S при x = 0, то это равно (-xj)
                                low_part = low_part * (first_kv - second_kv); // знаменатель
                            }
                        }

                        // В интерполяции Лагранжа есть деления, а нам нужны целые числа. Рассмотрим
                        //  a * a^(-1) = 1
                        // Тогда
                        //  a * a^(-1) = 1 (mod p)
                        // т.к. у нас вычисления по модулю, то мы можем заменить a^(-1) на другое число, дающее такой же результат
                        //  a * b = 1 (mod p).
                        // b может быть найдено по алгоритму Евклида в виде 'ax + by = НОД(a,b)'  (*1)
                        // таким образом, a = low_part и b = p, получаем НОД и 2 числа, x and y.
                        // т.к. НОД(простое_число, любое_число) = 1, заменим (*1):
                        // low_part * x + p*y = 1
                        // В таком случае y всегда будет '0', следовательно,
                        // low_part*x = 1
                        // где x равно (low_part)^(-1)
                        iVector temp = Evklid(low_part, p);

                        low_part = MathMod(temp.y, p);

                        high_part = MathMod((high_part * low_part),
                            p); // let high part temporary storage all lart of 'li' (see lagrange interpolation)

                        leftText += String.Format("{0} + ", high_part * kv_pairs[first_kv][i]);

                        secret += high_part * kv_pairs[first_kv][i]; // summ_all( y * li )

                        high_part = 1;
                        low_part = 1;
                    }
                    leftText += String.Format(" = {0} mod {1}", secret, p);
                    secret = MathMod(secret, p); // значение кода по модулю p 
                    try
                    {
                        recoveredSecret.Add((byte) secret);
                        leftText += String.Format(" = {0}", (byte) secret);
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(String.Format("Получено значение {0}, по которому нельзя восстановить символ",
                            secret));
                        return "Ошибка расшифровки";
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Введено недопустимое количество людей.");
                return "Ошибка расшифровки";
            }
            recoveredSecretArray = recoveredSecret.ToArray();
            char[] unicodeChars = new char[unicode.GetCharCount(recoveredSecretArray, 0, recoveredSecretArray.Length)];
            unicode.GetChars(recoveredSecretArray, 0, recoveredSecretArray.Length, unicodeChars, 0);
            return new string(unicodeChars); // Готово ^^
        }


        //--------------------------------------------------------------
        // Другие вспомогательные функции
        //--------------------------------------------------------------

        public BigInteger getRandom(int length)
        {
            Random random = new Random();
            random.Next();
            byte[] data = new byte[length];
            random.NextBytes(data);
            return new BigInteger(data);
        }

        public static BigInteger MathMod(BigInteger a, BigInteger b)
        {
            return (BigInteger.Abs(a * b) + a) % b;
        }

        public iVector Evklid(BigInteger a, BigInteger b)
        {
            iVector u = new iVector(a, 1, 0);
            iVector v = new iVector(b, 0, 1);
            iVector T = new iVector(0, 0, 0);
            BigInteger q = 0;

            while (v.x != 0)
            {
                q = u.x / v.x;

                T.x = MathMod(u.x, v.x);

                T.y = u.y - q * v.y;
                T.z = u.z - q * v.z;

                u.set(v); // u = v, but we need to CHANGE value, not CHANGE POINTER.
                v.set(T); // u = v, but we need to CHANGE value, not CHANGE POINTER.
            }

            return u;
        }
    }

    //Some abstract type of vector created for clearness of Evklid algorithm.
    public class iVector
    {
        public BigInteger x;
        public BigInteger y;
        public BigInteger z;

        public iVector()
        {
            x = 0;
            y = 0;
            z = 0;
        }

        public void set(iVector sec)
        {
            this.x = sec.x;
            this.y = sec.y;
            this.z = sec.z;
        }

        public iVector(BigInteger nx, BigInteger ny, BigInteger nz)
        {
            x = nx; y = ny; z = nz;
        }

        public iVector(int nx, int ny, int nz)
        {
            x = nx; y = ny; z = nz;
        }

        public string toString()
        {
            return "(" + x + ", " + y + ", " + z + ")";
        }
    }
}